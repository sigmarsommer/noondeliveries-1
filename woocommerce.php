<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>



<div id="primary" class="content-area">
  <div class="container">
    <?php
				woocommerce_content();
				
			?>
  </div>
  
      <div class="suggest-btn">
    
        <h4>Suggest a Restaurant</h4>
    
        <div class="green button">click here</div>
    </div>
  
  <div class="bottom-social-box">

	<div class="container">
		<div class="sixteen columns">
			<h3>Like Noon deliveries<br> on facebook</h3>
			<div data-show-border="true" data-stream="false" data-header="true" data-show-faces="true" data-colorscheme="light" data-height="210px" data-href="https://www.facebook.com/FacebookDevelopers" class="fb-like-box"></div>


		</div>
	</div>



</div>

<?php

get_footer();
?>
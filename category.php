<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div id="top"></div>
<div class="breadcrumb top_space_padding">

	<div class="container">

		<div class="sixteen columns">

	<ul>
		<li class="back-home donepage"><a href="">Home</a></li>
		<li class="onpage donepage"><a href="">Food Deliveries Punda</a></li>
	</ul>
</div>

</div>

</div>
<div class="location-header">

	<div class="container">

		<div class="sixteen columns location-head"><h4>Punda <span>food deliveries and takeaway</span></h4></div>

		<div class="sixteen columns location-sub-info"><span>36 restaurants</span> deliver in <span>Punda</span> <a href="">Change your location</a></div>

	</div>

</div>
<div class="venues">


	<div class="container">

		<div class="four columns">

			<ul class="venue-sections-list">

				<li class="selected"><a href="">All<span>36</span></a></li>
				<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'side' ) ); ?>
				


			</ul>

		</div>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .archive-header -->

			<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next page navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();